﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSM
{
    private FSMState curState;
    private Dictionary<string, FSMState> states;

    public FSMState CurState { get => curState; }

    public FSM()
    {
        this.curState = null;
        this.states = new Dictionary<string, FSMState>();
    }

    public void Clear()
    {
        this.curState = null;
        this.states.Clear();
    }

    public void AddState(string key)
    {
        if (this.states.ContainsKey(key))
            return;
        FSMState st = new FSMState();
        st.Init(this, key);
        this.states[key] = st;
    }

    public void AddState(FSMState st)
    {
        if (this.states.ContainsKey(st.Key))
            return;
        this.states[st.Key] = st;
    }

    public void RemoveState(string key)
    {
        if (!this.states.ContainsKey(key))
            return;
        this.states.Remove(key);
    }

    public void Start(string key)
    {
        if (!this.states.ContainsKey(key))
            return;
        OnChangeState(this.states[key]);
    }

    public void OnChangeState(FSMState st)
    {
        if (this.curState != null)
        {
            this.curState.OnExit();
        }
        this.curState = st;
        this.curState.OnEnter();
    }

    public void ChangeStateByKey(string key)
    {
        if (this.curState == null)
            return;
        FSMState nextState = this.curState.GetState(key);
        if (nextState != null)
        {
            OnChangeState(nextState);
        }
    }

    public void OnUpdate(float dt)
    {
        if(this.curState != null)
        {
            this.curState.OnUpdate(dt);
        }
    }



}
