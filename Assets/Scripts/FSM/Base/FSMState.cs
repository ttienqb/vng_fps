﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMState: MonoBehaviour
{
    private List<FSMAction> actions;
    private Dictionary<string, FSMState> transtitions;
    private FSM owner;
    private string key;

    public List<FSMAction> Actions { get => actions; }
    public string Key { get => key; }
    public FSM Owner { get => owner;  }

    public virtual void Init(FSM v, string k)
    {
        this.actions = new List<FSMAction>();
        this.transtitions = new Dictionary<string, FSMState>();
        this.owner = v;
        this.key = k;
    }

    public virtual void OnEnter()
    {
        for (int i = 0; i < this.actions.Count; i++)
        {
            if (this.actions[i] == null)
                continue;
            this.actions[i].OnEnter();
        }
    }

    public virtual void OnUpdate(float dt)
    {
        for (int i = 0; i < this.actions.Count; i++)
        {
            if (this.actions[i] == null)
                continue;
            this.actions[i].OnUpdate(dt);
        }
    }

    public virtual void OnExit()
    {
        for (int i = 0; i < this.actions.Count; i++)
        {
            if (this.actions[i] == null)
                continue;
            this.actions[i].OnExit();
        }
    }

    public virtual void Clear()
    {
        this.actions.Clear();
        this.transtitions.Clear();
    }

    public void RegisterTransition(string key, FSMState st)
    {
        if (this.transtitions.ContainsKey(key))
            return;
        this.transtitions[key] = st;
    }

    public void UnregisterTransition(string key)
    {
        if (!this.transtitions.ContainsKey(key))
            return;
        this.transtitions.Remove(key);
    }

    public FSMState GetState(string key)
    {
        if (this.transtitions.ContainsKey(key))
        {
            return this.transtitions[key];
        }

        return null;
    }

    public void AddAction(FSMAction act)
    {
        if (this.actions.Contains(act))
            return;
        this.actions.Add(act);
    }

    public void RemoveAction(FSMAction act)
    {
        if (!this.actions.Contains(act))
            return;
        this.actions.Remove(act);
    }

}
