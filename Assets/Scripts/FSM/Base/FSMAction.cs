﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FSMAction 
{
    public virtual void OnEnter()
    {
    }

    public virtual void OnExit()
    {
    }

    public virtual void OnUpdate(float dt)
    {
    }
}
