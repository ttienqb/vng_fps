﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AKWeaponState : WeaponFSMState
{

    public override void Clear()
    {
        base.Clear();
    }

    public override void Init(FSM v, string k)
    {
        base.Init(v, k);
    }

    public override void OnEnter()
    {
        base.OnEnter();

        MessageManager.Instance.Broadcast(new UISwitchMessage()
        {
            Name = Constant.NAME_WEAPON_AK
        });
    }

    public override void OnExit()
    {
        base.OnExit();

    }

    public override void OnUpdate(float dt)
    {
        base.OnUpdate(dt);
    }

    public override void Reload()
    {
        base.Reload();
    }

    public override void Fire()
    {
        base.Fire();
    }

    public override void ChargeFull()
    {
        base.ChargeFull();
    }
}
