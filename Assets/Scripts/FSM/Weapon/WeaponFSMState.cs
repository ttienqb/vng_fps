﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponFSMState : FSMState
{
    public Transform tfCamContainer;
    public Animator Ani;
    public GameObject gTarget;
    public int DefaultBullet;
    public float DefaultReload;


    protected float reloadTime;
    protected int curBullet;
    private WeaponController own;


    public WeaponController Own { get => own; }

    public override void Clear()
    {
        base.Clear();
    }

    public override void Init(FSM v, string k)
    {
        base.Init(v, k);
        this.gTarget.SetActive(false);
        this.curBullet = this.DefaultBullet;
    }


    public override void OnEnter()
    {
        //Debug.Log(" OnEnter =====  " + this.Key);
        this.Own.fpsCam.transform.SetParent(this.tfCamContainer);
        this.gTarget.SetActive(true);
        this.Ani.SetTrigger(Constant.ANI_TRIGGER_IN);
        this.reloadTime = 0;

        BroadCast_UIBullet();


    }

    public override void OnExit()
    {
        //Debug.Log(" OnExit =====  " + this.Key);
        this.gTarget.SetActive(false);
        //this.Ani.SetTrigger(Constant.ANI_TRIGGER_OUT);
    }

    public override void OnUpdate(float dt)
    {
        base.OnUpdate(dt);

        if (this.reloadTime > 0)
        {
            this.reloadTime -= dt;
            if (this.reloadTime <= 0)
            {
                ChargeFull();
            }
        }
    }

    public void Init(FSM v, string k, WeaponController c)
    {
        Init(v, k);
        this.own = c;
    }


    public virtual void Fire()
    {
        if (curBullet <= 0)
        {
            MessageManager.Instance.Broadcast(new InputMessage()
            {
                NameInput = Constant.INPUT_RELOAD
            });
            MessageManager.Instance.Broadcast(new AnyMessage()
            {
                Name = Constant.MESSAGE_ANY_OUT_OF_BULLET_UI
            });
            return;
        }
        AudioManager.Instance.PlaySound(SoundGame.Sound_Shot);

        this.curBullet--;
        BroadCast_UIBullet();
        MessageManager.Instance.Broadcast(new AnyMessage()
        {
            Name = Constant.MESSAGE_ANY_FIRE_UI
        });

        OnShot();
    }

    public virtual void ChargeFull()
    {
        this.curBullet = this.DefaultBullet;
        BroadCast_UIBullet();
    }

    public virtual void Reload()
    {
        AudioManager.Instance.StopAllSounds();
        AudioManager.Instance.PlaySound(SoundGame.Sound_Reload);

        this.Ani.SetTrigger(Constant.ANI_TRIGGER_RELOAD);
        if (this.curBullet < this.DefaultBullet)
        {
            this.reloadTime = this.DefaultReload;
        }
    }

    protected void BroadCast_UIBullet()
    {
        MessageManager.Instance.Broadcast(new UIBulletMessage()
        {
            CurBullet = this.curBullet,
            TotalBullet = this.DefaultBullet
        });
    }

    public void SetOnFPSCam(bool isOn)
    {
        this.gTarget.SetActive(isOn);
        if (isOn)
        {
            BroadCast_UIBullet();
        }
    }

    private void OnShot()
    {
        // check ray cast ememy

        RaycastHit hit;
        Transform tfCam = this.Own.fpsCam.transform;
        if (Physics.Raycast(tfCam.position,
            tfCam.forward, out hit, Constant.CHAR_FIRE_RANGE))
        {
            if (hit.transform != null)
            {
                HeadEmeny head = hit.collider.transform.GetComponent<HeadEmeny>();
                if (head != null)
                {
                    head.Owner.BeHitByHeadShot(this.own.Player);
                    this.own.Player.Shoot(head.Owner);
                }
                else
                {
                    BaseObject scr = hit.transform.GetComponent<BaseObject>();
                    if (scr != null)
                    {
                        scr.BeHit(this.own.Player);
                        this.own.Player.Shoot(scr);
                    }
                }
                //EffectManager.Instance.GenerateShootEffect(hit.transform);
            }
        }
        this.Ani.SetTrigger(Constant.ANI_TRIGGER_FIRE);
    }

}
