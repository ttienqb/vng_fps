﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateEnd : GameFSMState
{
    public override void Clear()
    {
        base.Clear();
    }

    public override void Init(FSM v, string k)
    {
        base.Init(v, k);
    }

    public override void OnEnter()
    {
        base.OnEnter();
        SetFPSCamera(false);

        if (this.player.CurHp > 0)
        {
            UIManager.Instance.ShowPopup<PopupEnd>("YOU VICTORY !!!");
        } else
        {
            UIManager.Instance.ShowPopup<PopupEnd>("YOU LOSE");
        }
    }

    public override void OnExit()
    {
        base.OnExit();
    }

    public override void OnUpdate(float dt)
    {
        base.OnUpdate(dt);
    }

   

}