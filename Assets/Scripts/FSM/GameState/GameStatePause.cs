﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatePause : GameFSMState
{
    public override void Clear()
    {
        base.Clear();
    }

    public override void Init(FSM v, string k)
    {
        base.Init(v, k);
    }

    public override void OnEnter()
    {
        base.OnEnter();

        SetFPSCamera(false);
        UIManager.Instance.ShowPopup<PopupPause>(this.fpsCam);
    }

    public override void OnExit()
    {
        base.OnExit();
    }

    public override void OnUpdate(float dt)
    {
        base.OnUpdate(dt);
    }
}
