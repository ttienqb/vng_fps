﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateSwitch : GameFSMState
{
    public Transform[] movePoints;


    public override void Clear()
    {
        base.Clear();
    }

    public override void Init(FSM v, string k)
    {
        base.Init(v, k);
    }

    public override void OnEnter()
    {
        base.OnEnter();
        SetFPSCamera(false);
        OnAnimationSwitchWave();
    }

    public override void OnExit()
    {
        base.OnExit();
    }

    public override void OnUpdate(float dt)
    {
        base.OnUpdate(dt);
    }

    private void OnAnimationSwitchWave()
    {
        DOTween.Kill(this.GetInstanceID().ToString());
        Sequence seq = DOTween.Sequence();
        seq.SetId(this.GetInstanceID().ToString());
        
        float dt = 3f;
        Vector3 nextPos = this.map.GetCurWave().CharMovePoint.position;

        seq.AppendInterval(1);
        seq.AppendCallback(delegate ()
        {
            this.player.SetAnimation(Constant.ANI_TRIGGER_IS_RUN);
        });
        seq.Append(player.transform.DOMove(nextPos, dt));
        seq.AppendCallback(delegate ()
        {
            this.player.SetAnimation(Constant.ANI_TRIGGER_IS_IDLE);
            UIManager.Instance.ShowPopup<PopupWave>();
        });
        seq.AppendInterval(1.5f);
        seq.OnComplete(delegate ()
        {
            UIManager.Instance.HidePopup<PopupWave>();
            this.Owner.ChangeStateByKey(Constant.FSM_TRANSITION_GAME_PLAY);
        });
    }

}