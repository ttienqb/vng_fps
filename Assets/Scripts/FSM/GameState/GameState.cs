﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFSMState : FSMState
{
    public CameraFollow mainCam;
    public FPSCamera fpsCam;
    public PlayerObject player;

    protected MapController map;

    public override void Clear()
    {
        base.Clear();
    }

    public override void Init(FSM v, string k)
    {
        base.Init(v, k);
    }

    public override void OnEnter()
    {
        //Debug.Log(" OnEnter " + this.Key + " - ");
        base.OnEnter();
    }

    public override void OnExit()
    {
        //Debug.Log(" OnExit " + this.Key + " - " );
        base.OnExit();
    }

    public override void OnUpdate(float dt)
    {
        base.OnUpdate(dt);
    }

    public virtual void Init(FSM v, string k, MapController m)
    {
        Init(v, k);
        this.map = m;
    }

    public void SetFPSCamera(bool isOn)
    {
        //Debug.Log(" SetFPSCamera " + this.Key + " - " + isOn);
        this.mainCam.gameObject.SetActive(!isOn);
        this.fpsCam.gameObject.SetActive(isOn);
        this.player.SetOnFPSCam(isOn);

        if (isOn)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }

}
