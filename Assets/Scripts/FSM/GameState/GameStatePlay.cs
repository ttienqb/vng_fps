﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatePlay : GameFSMState
{

    private List<BaseObject> objects = new List<BaseObject>();

    public override void Clear()
    {
        base.Clear();

        for (int i = 0; i < this.objects.Count; i++)
        {
            this.objects[i].ObjectDestroy();
        }
        this.objects.Clear();
    }

    public override void Init(FSM v, string k)
    {
        base.Init(v, k);

        this.player.Init();
        if (!this.objects.Contains(this.player))
        {
            this.objects.Add(this.player);
        }

    }

    public override void OnEnter()
    {
        base.OnEnter();

        if (MessageManager.HasInstance)
        {
            MessageManager.Instance.Register<AnyMessage>(OnListen_Any);
            MessageManager.Instance.Register<InputMessage>(OnListen_Input);
        }

        SetFPSCamera(true);
        this.map.GetCurWave().ActiveWave(true);
        if (!this.objects.Contains(this.map.GetCurWave()))
        {
            this.objects.Add(this.map.GetCurWave());
        }
    }

    public override void OnExit()
    {
        base.OnExit();

        if (MessageManager.HasInstance)
        {
            MessageManager.Instance.Unregister<AnyMessage>(OnListen_Any);
            MessageManager.Instance.Unregister<InputMessage>(OnListen_Input);

        }
    }

    public override void OnUpdate(float dt)
    {
        base.OnUpdate(dt);

        for (int i = 0; i < this.objects.Count; i++)
        {
            if (objects[i] != null)
            {
                this.objects[i].OnUpdate(Time.deltaTime);
            }
        }
    }
  

    void OnListen_Any(AnyMessage val)
    {
        if (val.Name == Constant.MESSAGE_ANY_PLAYER_DIE)
        {
            OnPlayerDie();
        }
        else if (val.Name == Constant.MESSAGE_ANY_COMPLETE_WAVE)
        {
            OnCompleteWave();
        }
        else { };
    }

    void OnListen_Input(InputMessage msg)
    {
        if (msg.NameInput == Constant.INPUT_PAUSE)
        {
            OnPauseGame();
        }
        else { }
    }

    private void OnPlayerDie()
    {
        this.map.GameOver();
    }

    private void OnCompleteWave()
    {
        if (this.objects.Contains(this.map.GetCurWave()))
        {
            this.objects.Remove(this.map.GetCurWave());
        }

        this.map.CompleteWave();

    }

    void OnPauseGame()
    {
        this.Owner.ChangeStateByKey(Constant.FSM_TRANSITION_GAME_PAUSE);
    }

}