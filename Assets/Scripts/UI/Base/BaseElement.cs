﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseElement : MonoBehaviour
{
    protected CanvasGroup canvasGroup;

    public CanvasGroup CanvasGroup
    {
        get
        {
            return canvasGroup;
        }
    }

    private bool isHide;

    public bool IsHide
    {
        get
        {
            return isHide;
        }
    }

    public virtual void Init()
    {
        this.gameObject.SetActive(true);
        RectTransform rect = this.GetComponent<RectTransform>();
        rect.pivot = new Vector2(0.5f, 0.5f);
        rect.anchorMin = Vector2.zero;
        rect.anchorMax = Vector2.one;
        rect.offsetMax = Vector2.zero;
        rect.offsetMin = Vector2.zero;

        if (!this.gameObject.GetComponent<CanvasGroup>())
        {
            this.gameObject.AddComponent<CanvasGroup>();
        }
        canvasGroup = this.gameObject.GetComponent<CanvasGroup>();

        Hide();
    }

    public virtual void Show(object data)
    {
        this.gameObject.SetActive(true);
        isHide = false;
        SetActiveGroupCanvas(true);
    }

    public virtual void Hide()
    {
        isHide = true;
        SetActiveGroupCanvas(false);
    }

    public virtual void Reset() { }

    private void SetActiveGroupCanvas(bool isAct)
    {
        canvasGroup.blocksRaycasts = isAct;
        canvasGroup.alpha = isAct ? 1 : 0;
    }

    public virtual void OnClickedBackButton()
    {

    }

}
