﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BasePopup : BaseElement
{
    public bool IgnoreBGColor = false;

    public override void Hide()
    {
        base.Hide();
    }

    public override void Init()
    {
        base.Init();
        if(!IgnoreBGColor)
        {
            Image img = this.GetComponent<Image>();
            if (img != null)
            {
                img.color = new Color(0, 0, 0, 0.5f);
            }
        }
    }

    public override void OnClickedBackButton()
    {
        base.OnClickedBackButton();
    }

    public override void Reset()
    {
        base.Reset();
    }

    public override void Show(object data)
    {
        base.Show(data);
    }
}
