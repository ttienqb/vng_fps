﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseScreen : BaseElement
{
    public override void Hide()
    {
        base.Hide();
    }

    public override void Init()
    {
        base.Init();
    }

    public override void Reset()
    {
        base.Reset();
    }

    public override void Show(object data)
    {
        base.Show(data);
    }
}
