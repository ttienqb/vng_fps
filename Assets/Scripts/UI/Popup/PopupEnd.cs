﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupEnd : BasePopup
{
    public Text lblTitle;

    public void OnClickedCloseButton()
    {
        AudioManager.Instance.PlaySound(SoundGame.Sound_Button);

        //Hide();
    }

    public override void Hide()
    {
        base.Hide();
    }

    public override void Init()
    {
        base.Init();
    }

    public override void OnClickedBackButton()
    {
        base.OnClickedBackButton();
    }

    public override void Reset()
    {
        base.Reset();
    }

    public override void Show(object data)
    {
        base.Show(data);

        if(data != null)
        {
            lblTitle.text = "" + data;
        }
    }
}
