﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupWave : BasePopup
{
    public Text lblTitle, lblContent;

    public override void Hide()
    {
        base.Hide();
    }

    public override void Init()
    {
        base.Init();
    }

    public override void OnClickedBackButton()
    {
        base.OnClickedBackButton();
    }

    public override void Reset()
    {
        base.Reset();
    }

    public override void Show(object data)
    {
        base.Show(data);
        MapController map = LevelManager.Instance.map;
        this.lblTitle.text = "WAVE " + map.CurWave;
        if (map.waves.Length <= map.CurWave)
        {
            this.lblTitle.text = "WAVE FINAL  !!! ";
        }
        WaveObject wave = map.GetCurWave();

        string content = "MISSION: \n";
        if (wave.walkEnenimes > 0)
        {
            content += "\nKILL " + wave.walkEnenimes + " WALK ENEMIES.";
        }
        if (wave.gunEnenimes > 0)
        {
            content += "\nKILL " + wave.gunEnenimes + " GUN ENEMIES.";
        }
        this.lblContent.text = content;
    }
}
