﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupPause : BasePopup
{
    public Slider slSmooth, slSensitivity;

    private FPSCamera cam;

    public void OnClickedResumeButton()
    {
        AudioManager.Instance.PlaySound(SoundGame.Sound_Button);
        Hide();
        LevelManager.Instance.map.ResumeGame();
    }

    public override void Hide()
    {
        base.Hide();

        if(this.cam != null)
        {
            this.cam.sensitivity = new Vector2(slSensitivity.value, slSensitivity.value);
            this.cam.smoothing = new Vector2(slSmooth.value, slSmooth.value);
        }
    }

    public override void Init()
    {
        base.Init();
    }

    public override void OnClickedBackButton()
    {
        base.OnClickedBackButton();
    }

    public override void Reset()
    {
        base.Reset();
    }

    public override void Show(object data)
    {
        base.Show(data);

        if(data != null)
        {
            this.cam = (FPSCamera)data;
        }

        if (this.cam != null)
        {
            slSensitivity.value = this.cam.sensitivity.x;
            slSmooth.value = this.cam.smoothing.x;
        }
    }
}