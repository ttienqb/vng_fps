﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupGuide : BasePopup
{
    public void OnClickedStartButton()
    {
        AudioManager.Instance.PlaySound(SoundGame.Sound_Button);

        Hide();
        LevelManager.Instance.StartGame();
    }
 
    public override void Hide()
    {
        base.Hide();
    }

    public override void Init()
    {
        base.Init();
    }

    public override void OnClickedBackButton()
    {
        base.OnClickedBackButton();
    }

    public override void Reset()
    {
        base.Reset();
    }

    public override void Show(object data)
    {
        base.Show(data);
    }
}
