﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    public GameObject gCrosshairs;
    public Text lblGunName, lblStock, lblHp, lblGuner, lblWalker;
    public Image imgGunIcon;
    public Slider slHp;
    public Sprite[] ListGunSpr;

    void Awake()
    {
        if (MessageManager.HasInstance)
        {
            MessageManager.Instance.Register<AnyMessage>(OnListen_AnyMessage);
            MessageManager.Instance.Register<UIBulletMessage>(OnListen_BulletUI);
            MessageManager.Instance.Register<UIHpMessage>(OnListen_HpUI);
            MessageManager.Instance.Register<UISwitchMessage>(OnListen_SwitchUI);
            MessageManager.Instance.Register<UIMissionMessage>(OnListen_MissionUI);

        }
    }

    void OnDestroy()
    {
        if (MessageManager.HasInstance)
        {
            MessageManager.Instance.Unregister<AnyMessage>(OnListen_AnyMessage);
            MessageManager.Instance.Unregister<UIBulletMessage>(OnListen_BulletUI);
            MessageManager.Instance.Unregister<UIHpMessage>(OnListen_HpUI);
            MessageManager.Instance.Unregister<UISwitchMessage>(OnListen_SwitchUI);
            MessageManager.Instance.Unregister<UIMissionMessage>(OnListen_MissionUI);

        }
    }

    public void OnClickedPauseButton()
    {
        AudioManager.Instance.PlaySound(SoundGame.Sound_Button);
        InputManager.Instance.BroadcastInput(Constant.INPUT_FIRE);
    }

    public void OnClickedFireButton()
    {
        InputManager.Instance.BroadcastInput(Constant.INPUT_FIRE);
    }

    public void OnClickedReloadButton()
    {
        InputManager.Instance.BroadcastInput(Constant.INPUT_RELOAD);
    }

    public void OnClickedSwitchButton()
    {
        InputManager.Instance.BroadcastInput(Constant.INPUT_SWITCH);
    }

    void OnListen_BulletUI(UIBulletMessage msg)
    {
        this.lblStock.text = msg.CurBullet + "/" + msg.TotalBullet;
    }

    void OnListen_HpUI(UIHpMessage msg)
    {
        this.lblHp.text = "HP: " + msg.CurHp + "/" + msg.TotalHp;
        this.slHp.value = ((float)msg.CurHp / (float)msg.TotalHp);
    }

    void OnListen_MissionUI(UIMissionMessage msg)
    {
        this.lblWalker.text = "Kill Walker: " + msg.WalkEnemies;
        this.lblGuner.text = "Kill Gunner: " + msg.GunEnemies;

    }

    void OnListen_SwitchUI(UISwitchMessage msg)
    {
        if (msg.Name == Constant.NAME_WEAPON_GUN)
        {
            this.lblGunName.text = Constant.NAME_WEAPON_GUN;
            this.imgGunIcon.sprite = this.ListGunSpr[0];
        }
        else
        {
            this.lblGunName.text = Constant.NAME_WEAPON_AK;
            this.imgGunIcon.sprite = this.ListGunSpr[1];
        }
    }

    void OnListen_AnyMessage(AnyMessage msg)
    {
        if (msg.Name == Constant.MESSAGE_ANY_FIRE_UI)
        {
            OnAnimationFire();
        }
        else if (msg.Name == Constant.MESSAGE_ANY_OUT_OF_BULLET_UI)
        {
            OnAnimationOutOfBullet();
        }
        else { };
    }

    private void OnAnimationFire()
    {
        float scale = this.gCrosshairs.transform.localScale.x + 0.3f;
        this.gCrosshairs.transform.localScale = Vector3.one * scale;

        DOTween.Kill(this.gCrosshairs.GetInstanceID().ToString());
        Sequence seq = DOTween.Sequence();
        seq.SetId(this.gCrosshairs.GetInstanceID().ToString());

        seq.Append(this.gCrosshairs.transform.DOScale(Vector3.one, 0.5f));
        seq.OnComplete(delegate ()
        {
            this.gCrosshairs.transform.localScale = Vector3.one;
        });
    }

    private void OnAnimationOutOfBullet()
    {
        this.lblStock.transform.localScale = Vector3.one * 2f;
        DOTween.Kill(this.lblStock.GetInstanceID().ToString());
        Sequence seq = DOTween.Sequence();
        seq.SetId(this.lblStock.GetInstanceID().ToString());
        seq.Append(this.lblStock.transform.DOScale(Vector3.one, 0.5f));
        seq.OnComplete(delegate ()
        {
            this.lblStock.transform.localScale = Vector3.one;
        });
    }


}
