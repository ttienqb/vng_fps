﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider;
    public TextMeshProUGUI lblHp;

    private int maxHp;


    public void Init(int val)
    {
        this.maxHp = val;
        this.slider.value = 1;
        this.lblHp.text = "" + val;
    }

    public void SetHp(int val)
    {
        this.slider.value = ((float)val / (float)this.maxHp);
        this.lblHp.text = "" + val;
        if (val <= 0)
        {
            this.lblHp.text = "";
        }
    }
}
