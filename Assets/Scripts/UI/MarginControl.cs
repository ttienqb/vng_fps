﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MarginControl : MonoBehaviour
{
    public Image[] marginImages;
    public Image imgApply;

    void Awake()
    {
        foreach (var item in marginImages)
        {
            item.color = imgApply.color;
        }
    }
}
