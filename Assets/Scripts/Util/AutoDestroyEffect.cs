﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyEffect : MonoBehaviour
{
    public float DestroyTime;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DelayToHandle(DestroyTime));
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator DelayToHandle(float dt)
    {
        yield return new WaitForSeconds(dt);
        Destroy(this.gameObject);
    }
}
