﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyUtils
{

    public static void LookAt2D(Transform target, Vector3 vc)
    {
        target.rotation = Quaternion.LookRotation(vc);
        target.eulerAngles = new Vector3(target.eulerAngles.x, target.eulerAngles.y);
    }


}
