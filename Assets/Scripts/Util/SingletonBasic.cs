﻿
public class SingletonBasic<T> where T : new()
{
    protected bool isInited = false;

    private static object singletonLock = new object();
    private static T instance = default(T);

    public static bool HasInstance
    {
        get
        {
            return instance != null;
        }
    }

    public static T Instance
    {
        get
        {
            lock (singletonLock)
            {
                if (instance == null)
                {
                    instance = new T();
                }
                return instance;
            }
        }
    }

    public void Init(bool forceInit = false)
    {
        if (isInited && !forceInit)
            return;
        isInited = true;
        OnInited();
    }

    /// <summary>
    ///  Init only 1 time
    /// </summary>
    public virtual void OnInited()
    {

    }

    public static void DestroyInstance()
    {
        instance = default(T);
    }
}