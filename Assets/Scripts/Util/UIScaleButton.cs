using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;

[RequireComponent(typeof(EventTrigger))]
public class UIScaleButton : MonoBehaviour
{
    public float from = 1;
    public float to = 1.1f;

    private Button myBtn;

    void Awake()
    {
        myBtn = this.gameObject.GetComponent<Button>();
        this.GetComponent<RectTransform>().localScale = Vector3.one * from;
        if(myBtn != null)
        {
            myBtn.transition = Selectable.Transition.None;
        }
    }

    void Start()
    {


        EventTrigger trigger = this.GetComponent<EventTrigger>();

        // POINTER DOWN EVENT
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener((eventData) => this.OnPointerDown());
        trigger.triggers.Add(entry);

        // POINTER UP EVENT
        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener((eventData) => this.OnPointerUp());
        trigger.triggers.Add(entry);
    }

    void OnPointerDown()
    {
        this.GetComponent<RectTransform>().localScale = Vector3.one * to;
    }

    void OnPointerUp()
    {
        this.GetComponent<RectTransform>().localScale = Vector3.one * from;
    }
}
