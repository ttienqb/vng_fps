﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ObjectType
{
    None,
    Player = 1,
    Enemy = 10,
    Enemy_Gun = 11,
    Wave = 20,
}

public enum CollisionType
{
    None,
    Player = 1,
    Enemy = 10,
    Obstacle = 30,
    Ground = 40,
}

public enum EnemyState
{
    Idle,
    Move,
    Fire,
    KnockBack,
    Die,
    Atk,
}

public enum PlayerState
{
    Idle,
    Reload,
    Switch,
    Fire
}

