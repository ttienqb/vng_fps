﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constant
{
    // play pref
    public const string PLAY_PREF_SOUND = "PLAY_PREF_SOUND";
    public const string PLAY_PREF_MUSIC = "PLAY_PREF_MUSIC";

    public const int DEFAULT_HEIGHT_SCREEN = 1920;
    public const int DEFAULT_WIDTH_SCREEN = 1080;

    //input
    public const string INPUT_FIRE = "INPUT_FIRE";
    public const string INPUT_RELOAD = "INPUT_RELOAD";
    public const string INPUT_SWITCH = "INPUT_SWITCH";
    public const string INPUT_PAUSE = "INPUT_PAUSE";


    // enemy
    public const float ENEMY_SPEED = 4;
    public const float ENEMY_AVOID = 0.5f;
    public const float ENEMY_SPACE_ATK = 3;
    public const float ENEMY_KNOCK_DISTANCE = 0.2f;
    public const float ENEMY_TIME_ATK = 1;

    // mesage
    public const string MESSAGE_ANY_FIRE_UI = "MESSAGE_ANY_FIRE_UI";
    public const string MESSAGE_ANY_OUT_OF_BULLET_UI = "MESSAGE_ANY_OUT_OF_BULLET_UI";
    public const string MESSAGE_ANY_PLAYER_DIE = "MESSAGE_ANY_PLAYER_DIE";
    public const string MESSAGE_ANY_COMPLETE_WAVE = "MESSAGE_ANY_COMPLETE_WAVE";


    // animation
    public const string ANI_TRIGGER_RELOAD = "IsReload";
    public const string ANI_TRIGGER_FIRE = "IsFire";
    public const string ANI_TRIGGER_OUT = "IsOut";
    public const string ANI_TRIGGER_IN = "IsIn";
    public const string ANI_TRIGGER_IS_RUN = "IsRun";
    public const string ANI_TRIGGER_IS_IDLE = "IsIdle";


    // character
    public const float CHAR_TIME_FIRE = 0.1f;
    public const float CHAR_TIME_RELOAD = 1f;
    public const float CHAR_FIRE_RANGE = 100f;
    public const float CHAR_TIME_BLOCK_ATK = 0.5f;

    // FSM
    public const string FSM_TRANSITION_WEAPON_GUN = "FSM_TRANSITION_WEAPON_GUN";
    public const string FSM_TRANSITION_WEAPON_AK = "FSM_TRANSITION_WEAPON_AK";
    public const string FSM_TRANSITION_GAME_SWITCH = "FSM_TRANSITION_GAME_SWITCH";
    public const string FSM_TRANSITION_GAME_PAUSE = "FSM_TRANSITION_GAME_PAUSE";
    public const string FSM_TRANSITION_GAME_PLAY = "FSM_TRANSITION_GAME_PLAY";
    public const string FSM_TRANSITION_GAME_END = "FSM_TRANSITION_GAME_END";

    // ui 
    public const string NAME_WEAPON_GUN = "Hand Gun";
    public const string NAME_WEAPON_AK = "AK ";



}
