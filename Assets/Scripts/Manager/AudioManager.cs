﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SoundGame
{
    Sound_Button,
    Sound_BeHit,
    Sound_Lose,
    Sound_Reload,
    Sound_Shot,
    Sound_Switch,
    Sound_Victory,
    Sound_Clear,
    Sound_Headshot,

}

public class AudioManager : Singleton<AudioManager>
{

    private AudioSource MusicdAudioSource = null;
    private AudioSource SoundAudioSource = null;

    private AudioClip sound_button;
    private AudioClip sound_behit;
    private AudioClip sound_lose;
    private AudioClip sound_reload;
    private AudioClip sound_shot;
    private AudioClip sound_switch;
    private AudioClip sound_victory;
    private AudioClip sound_clear;
    private AudioClip sound_headshot;


    private List<AudioClip> music_bg = new List<AudioClip>();

    public override void OnInited()
    {
        base.OnInited();
        this.MusicdAudioSource = this.gameObject.AddComponent<AudioSource>();
        this.MusicdAudioSource.playOnAwake = false;
        this.MusicdAudioSource.loop = true;
        this.MusicdAudioSource.spatialBlend = 0.0f;
        this.MusicdAudioSource.volume = PlayerPrefs.GetFloat(Constant.PLAY_PREF_MUSIC, 1f);
        this.SoundAudioSource = this.gameObject.AddComponent<AudioSource>();
        this.SoundAudioSource.playOnAwake = false;
        this.SoundAudioSource.loop = false;
        this.SoundAudioSource.spatialBlend = 0.0f;
        this.SoundAudioSource.volume = PlayerPrefs.GetFloat(Constant.PLAY_PREF_SOUND, 1f);

        if (!PlayerPrefs.HasKey(Constant.PLAY_PREF_SOUND))
        {
            UnmuteSound();
            UnmuteMusic();
        }
        LoadAudioResrouces();
    }

    private void LoadAudioResrouces()
    {
        sound_button = Resources.Load("Audios/sound_clickbutton") as AudioClip;
        sound_behit = Resources.Load("Audios/sound_behit") as AudioClip;
        sound_lose = Resources.Load("Audios/sound_lose") as AudioClip;
        sound_reload = Resources.Load("Audios/sound_reload") as AudioClip;
        sound_shot = Resources.Load("Audios/sound_shot") as AudioClip;
        sound_switch = Resources.Load("Audios/sound_switch") as AudioClip;
        sound_victory = Resources.Load("Audios/sound_victory") as AudioClip;
        sound_clear = Resources.Load("Audios/sound_clear") as AudioClip;
        sound_headshot = Resources.Load("Audios/sound_headshot") as AudioClip;
    }


    public void PlaySound(AudioClip clip)
    {
        this.SoundAudioSource.PlayOneShot(clip);
    }

    public void Stop()
    {
        this.SoundAudioSource.Stop();
    }

    public void PlayMusic(AudioClip clip)
    {
        this.MusicdAudioSource.clip = clip;
        this.MusicdAudioSource.Play();
    }

    public void PlaySound(SoundGame sound)
    {
        AudioClip sd = GetSound(sound);

        if (sd == null)
        {
            Debug.Log(sound + " not found !!!");
            LoadAudioResrouces();
        }
        else
        {
            PlaySound(sd);
        }
    }


    private AudioClip GetSound(SoundGame sound)
    {
        AudioClip result = null;
        switch (sound)
        {
            case SoundGame.Sound_Button:
                result = sound_button;
                break;
            case SoundGame.Sound_BeHit:
                result = sound_behit;
                break;
            case SoundGame.Sound_Lose:
                result = sound_lose;
                break;
            case SoundGame.Sound_Reload:
                result = sound_reload;
                break;
            case SoundGame.Sound_Shot:
                result = sound_shot;
                break;
            case SoundGame.Sound_Switch:
                result = sound_switch;
                break;
            case SoundGame.Sound_Victory:
                result = sound_victory;
                break;
            case SoundGame.Sound_Clear:
                result = sound_clear;
                break;
            case SoundGame.Sound_Headshot:
                result = sound_headshot;
                break;
        }
        return result;
    }

    public void PlayMusic(int index)
    {
        AudioClip tmp = null;

        if (index < music_bg.Count)
        {
            tmp = music_bg[index];
        }

        if (tmp == null)
        {
            Debug.Log("music " + index + " not found !!!");
            LoadAudioResrouces();
        }
        else
        {
            PlayMusic(tmp);
        }
    }

    public void StopMusic()
    {
        this.MusicdAudioSource.Stop();
    }

    public void MuteSound()
    {
        this.SoundAudioSource.volume = 0;
        PlayerPrefs.SetFloat(Constant.PLAY_PREF_SOUND, 0f);
        PlayerPrefs.Save();
    }

    public void UnmuteSound()
    {
        this.SoundAudioSource.volume = 1;
        PlayerPrefs.SetFloat(Constant.PLAY_PREF_SOUND, 1f);
        PlayerPrefs.Save();
    }

    public bool IsSoundMuted()
    {
        return this.SoundAudioSource.volume == 0f;
    }

    public bool IsMusicMuted()
    {
        return this.MusicdAudioSource.volume == 0f;
    }

    public void MuteMusic()
    {
        this.MusicdAudioSource.volume = 0;
        PlayerPrefs.SetFloat(Constant.PLAY_PREF_MUSIC, 0f);
        PlayerPrefs.Save();
    }

    public void UnmuteMusic()
    {
        this.MusicdAudioSource.volume = 1f;
        PlayerPrefs.SetFloat(Constant.PLAY_PREF_MUSIC, 1f);
        PlayerPrefs.Save();
    }

    public void Mute()
    {
        this.MuteSound();
        this.MuteMusic();
    }

    public void Unmute()
    {
        this.UnmuteSound();
        this.UnmuteMusic();
    }



    public bool IsMuted()
    {
        return this.MusicdAudioSource.volume == 0 ||
            this.SoundAudioSource.volume == 0;
    }

    public void SwitchState()
    {
        if (this.IsMuted())
            this.Unmute();
        else
            this.Mute();
    }


    public void SwitchMusicState()
    {
        if (this.IsMusicMuted())
            this.UnmuteMusic();
        else
            this.MuteMusic();
    }

    public void SwitchSoundState()
    {
        if (this.IsSoundMuted())
            this.UnmuteSound();
        else
            this.MuteSound();
    }

    public void StopAllSounds()
    {
        SoundAudioSource.Stop();
    }

    public void StopAllMusics()
    {
        MusicdAudioSource.Stop();
    }

    public void StopAll()
    {
        StopAllMusics();
        StopAllSounds();
    }


}
