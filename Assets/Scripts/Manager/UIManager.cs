﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIManager : Singleton<UIManager>
{
    public GameObject cScreen, cPopup, cNotify, cOverlap, container, cMargin;
    public Canvas MyCanvas;
    public EventSystem MyEvent;
    public Camera MyCam;



    private Dictionary<string, BaseScreen> listScreen = new Dictionary<string, BaseScreen>();
    private Dictionary<string, BasePopup> listPopup = new Dictionary<string, BasePopup>();
    private Dictionary<string, BaseNotify> listNotify = new Dictionary<string, BaseNotify>();


    private const string SCREEN_RESOURCES_PATH = "Prefabs/UI/Screen/";
    private const string POPUP_RESOURCES_PATH = "Prefabs/UI/Popup/";
    private const string NOTIFY_RESOURCES_PATH = "Prefabs/UI/Notify/";

    private const string NAME_SCREEN_CONTAINER = "CONTAINER_SCREEN";
    private const string NAME_POPUP_CONTAINER = "CONTAINER_POPUP";
    private const string NAME_OVERLAP_CONTAINER = "CONTAINER_OVERLAP";
    private const string NAME_NOTIFY_CONTAINER = "CONTAINER_NOTIFY";
    public const string NAME_UI_CONTAINER = "UI_CONTAINER";


    private BaseScreen curScreen;
    private BasePopup curPopup;
    private BaseNotify curNotify;

    public override void OnInited()
    {
        base.OnInited();
        this.cScreen.name = NAME_SCREEN_CONTAINER;
        this.cPopup.name = NAME_POPUP_CONTAINER;
        this.cOverlap.name = NAME_OVERLAP_CONTAINER;
        this.cNotify.name = NAME_NOTIFY_CONTAINER;
        this.container.name = NAME_UI_CONTAINER;

        //FixMultiResolution();
        Release();
    }

    private void FixMultiResolution()
    {
        float defaultRatio = (float)Constant.DEFAULT_HEIGHT_SCREEN / Constant.DEFAULT_WIDTH_SCREEN;
        float ratio = (float)Screen.height / Screen.width;

        if (Mathf.Abs(ratio - defaultRatio) > 0.1f)
        {
            RectTransform rt = this.container.GetComponent<RectTransform>();
            rt.sizeDelta = new Vector2(rt.sizeDelta.x * defaultRatio / ratio, rt.sizeDelta.y);
        }
    }

    public void ShowScreen<T>(object data = null) where T : BaseScreen
    {
        string nameScreen = typeof(T).ToString();
        BaseScreen result = null;

        if (curScreen != null
        && curScreen.GetType().ToString() == nameScreen)
        {
            result = curScreen;
        }

        if (result == null)
        {
            if (!listScreen.ContainsKey(nameScreen))
            {
                BaseScreen sceneScr = GetNewScreen<T>();
                if (sceneScr != null)
                {
                    listScreen.Add(typeof(T).ToString(), sceneScr);
                }
            }

            if (listScreen.ContainsKey(nameScreen))
            {
                result = listScreen[nameScreen];
            }
        }

        if (result != null && result.IsHide)
        {
            result.Reset();

            curScreen = result;
            HideAllNotifys();
            ClearPopup();
            HideAllScreens();
            result.transform.SetAsLastSibling();
            result.Show(data);
        }
    }

    private BaseScreen GetNewScreen<T>() where T : BaseScreen
    {
        string nameScreen = typeof(T).ToString();
        string path = SCREEN_RESOURCES_PATH + nameScreen;
        GameObject pfScreen = Resources.Load(path) as GameObject;
        if (pfScreen == null || !pfScreen.GetComponent<BaseScreen>())
        {
            throw new MissingReferenceException("Cant found " + nameScreen + " screen. !!!" + path);
        }

        GameObject ob = Instantiate(pfScreen) as GameObject;
        ob.transform.SetParent(this.cScreen.transform);
        ob.transform.localScale = Vector3.one;
        ob.transform.localPosition = Vector3.zero;
        ob.name = "SCREEN_" + nameScreen;
        BaseScreen sceneScr = ob.GetComponent<BaseScreen>();
        sceneScr.Init();
        return sceneScr;
    }

    public void HideAllScreens()
    {
        BaseScreen sceneScr = null;
        foreach (KeyValuePair<string, BaseScreen> item in listScreen)
        {
            sceneScr = item.Value;
            if (sceneScr == null
            || sceneScr.IsHide)
                continue;
            sceneScr.Hide();
        }
    }

    public BaseScreen GetExistScreen<T>() where T : BaseScreen
    {
        string nameScreen = typeof(T).ToString();
        if (listScreen.ContainsKey(nameScreen))
        {
            return listScreen[nameScreen];
        }
        throw new MissingReferenceException(nameScreen + " screen not exist !!");
    }

    public void ShowPopup<T>(object data = null) where T : BasePopup
    {
        string namePopup = typeof(T).ToString();
        BasePopup result = null;

        if (curPopup != null
        && curPopup.GetType().ToString() == namePopup)
        {
            result = curPopup;
        }

        if (result == null)
        {
            if (!listPopup.ContainsKey(namePopup))
            {
                BasePopup popupScr = GetNewPopup<T>();
                if (popupScr != null)
                {
                    listPopup.Add(typeof(T).ToString(), popupScr);
                }
            }
            if (listPopup.ContainsKey(namePopup))
            {
                result = listPopup[namePopup];
            }
        }

        if (result != null && result.IsHide)
        {
            result.Reset();
            curPopup = result;
            ClearPopup();
            result.transform.SetAsLastSibling();
            result.Show(data);
        }
    }

    private BasePopup GetNewPopup<T>() where T : BasePopup
    {
        string namePopup = typeof(T).ToString();
        string path = POPUP_RESOURCES_PATH + namePopup;
        GameObject pfPopup = Resources.Load(path) as GameObject;
        if (pfPopup == null || !pfPopup.GetComponent<BasePopup>())
        {
            throw new MissingReferenceException("Cant found " + namePopup + " popup. !!!" + path);
        }

        GameObject ob = Instantiate(pfPopup) as GameObject;
        ob.transform.SetParent(this.cPopup.transform);
        ob.transform.localScale = Vector3.one;
        ob.transform.localPosition = Vector3.zero;
        ob.name = "POPUP_" + namePopup;
        BasePopup popupScr = ob.GetComponent<BasePopup>();
        popupScr.Init();
        return popupScr;
    }

    public void HidePopup<T>() where T : BasePopup
    {
        string popupName = typeof(T).ToString();
        if (listPopup.ContainsKey(popupName))
        {
            listPopup[popupName].Hide();
        }
    }

    public void ClearPopup()
    {
        BasePopup popupScr = null;
        foreach (KeyValuePair<string, BasePopup> item in listPopup)
        {
            popupScr = item.Value;
            if (popupScr == null
            || popupScr.IsHide)
                continue;
            popupScr.Hide();
        }
    }

    public BasePopup GetExistPopup<T>() where T : BasePopup
    {
        string namePopup = typeof(T).ToString();
        if (listPopup.ContainsKey(namePopup))
        {
            return listPopup[namePopup];
        }
        throw new MissingReferenceException(namePopup + " popup not exist !!");
    }

    public void ShowNotify<T>(object data = null) where T : BaseNotify
    {
        string nameNotify = typeof(T).ToString();
        BaseNotify result = null;

        if (curNotify != null
        && curNotify.GetType().ToString() == nameNotify)
        {
            result = curNotify;
        }

        if (result == null)
        {
            if (!listNotify.ContainsKey(nameNotify))
            {
                BaseNotify notifyScr = GetNewNotify<T>();
                if (notifyScr != null)
                {
                    listNotify.Add(typeof(T).ToString(), notifyScr);
                }
            }
            if (listNotify.ContainsKey(nameNotify))
            {
                result = listNotify[nameNotify];
            }
        }

        if (result != null && result.IsHide)
        {
            result.Reset();
            curNotify = result;
            result.transform.SetAsLastSibling();
            result.Show(data);
        }
    }

    private BaseNotify GetNewNotify<T>() where T : BaseNotify
    {
        string nameNotify = typeof(T).ToString();
        string path = NOTIFY_RESOURCES_PATH + nameNotify;
        GameObject pfNotify = Resources.Load(path) as GameObject;
        if (pfNotify == null || !pfNotify.GetComponent<BaseNotify>())
        {
            throw new MissingReferenceException("Cant found " + nameNotify + " notify. !!!" + path);
        }

        GameObject ob = Instantiate(pfNotify) as GameObject;
        ob.transform.SetParent(this.cNotify.transform);
        ob.transform.localScale = Vector3.one;
        ob.transform.localPosition = Vector3.zero;
        ob.name = "NOTIFY_" + nameNotify;
        BaseNotify notifyScr = ob.GetComponent<BaseNotify>();
        notifyScr.Init();
        return notifyScr;
    }

    public void HideAllNotifys()
    {
        BaseNotify notifyScr = null;
        foreach (KeyValuePair<string, BaseNotify> item in listNotify)
        {
            notifyScr = item.Value;
            if (notifyScr == null
            || notifyScr.IsHide)
                continue;
            notifyScr.Hide();
        }
    }

    public BaseNotify GetExistNotify<T>() where T : BaseNotify
    {
        string notifyName = typeof(T).ToString();
        if (listNotify.ContainsKey(notifyName))
        {
            return listNotify[notifyName];
        }
        throw new MissingReferenceException(notifyName + " notify not exist !!");
    }

    public void HideNotify<T>() where T : BaseNotify
    {
        string notifyName = typeof(T).ToString();
        if (listNotify.ContainsKey(notifyName))
        {
            listNotify[notifyName].Hide();
        }
    }

    public void Release()
    {
        ReleaseScreen();
        ReleasePopup();
        ReleaseNotify();
    }

    public void ReleaseScreen()
    {
        this.listScreen.Clear();
        int index = 0;
        while (this.cScreen.transform.childCount > 0)
        {
            Destroy(this.cScreen.transform.GetChild(index).gameObject);
            index++;
            if (this.cScreen.transform.childCount <= index)
                break;
        }
    }

    public void ReleasePopup()
    {
        this.listPopup.Clear();
        int index = 0;
        while (this.cPopup.transform.childCount > 0)
        {
            Destroy(this.cPopup.transform.GetChild(index).gameObject);
            index++;
            if (this.cPopup.transform.childCount <= index)
                break;
        }
    }

    public void ReleaseNotify()
    {
        this.listNotify.Clear();
        int index = 0;
        while (this.cNotify.transform.childCount > 0)
        {
            Destroy(this.cNotify.transform.GetChild(index).gameObject);
            index++;
            if (this.cNotify.transform.childCount <= index)
                break;
        }
    }


}
