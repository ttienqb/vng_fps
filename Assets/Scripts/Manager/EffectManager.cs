﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectManager : Singleton<EffectManager>
{
    public GameObject gApplyDamage;
    public GameObject eff_shoot;


    public override void Awake()
    {
        base.Awake();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void OnInited()
    {
        base.OnInited();
    }

    public void ApplyDamageEffect()
    {
        this.gApplyDamage.SetActive(true);
        DOTween.Kill(this.gApplyDamage.GetInstanceID().ToString());
        Sequence seq = DOTween.Sequence();
        seq.SetId(this.gApplyDamage.GetInstanceID().ToString());

        seq.AppendInterval(0.2f);
        seq.OnKill(delegate ()
        {
            this.gApplyDamage.SetActive(false);
        });
    }

    public void GenerateShootEffect(Transform target)
    {
      
        GameObject ob = Instantiate(this.eff_shoot) as GameObject;
        ob.transform.position = target.transform.position;
        ob.SetActive(true);
    }

}
