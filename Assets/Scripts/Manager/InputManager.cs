﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : Singleton<InputManager>
{
    public override void Awake()
    {
        base.Awake();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            BroadcastInput(Constant.INPUT_FIRE);
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            BroadcastInput(Constant.INPUT_SWITCH);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            BroadcastInput(Constant.INPUT_RELOAD);
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            BroadcastInput(Constant.INPUT_PAUSE);
        }
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void OnInited()
    {
        base.OnInited();
    }

    public void BroadcastInput(string input)
    {
        FSMState scr = null;
        if (LevelManager.Instance.map.Fsm != null)
        {
            scr = LevelManager.Instance.map.Fsm.CurState;
        }
        if(scr != null && scr.Key == Constant.FSM_TRANSITION_GAME_PLAY)
        {
            MessageManager.Instance.Broadcast(new InputMessage()
            {
                NameInput = input
            });
        }
    }

}
