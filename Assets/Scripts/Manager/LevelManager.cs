﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : Singleton<LevelManager>
{
    public MapController map;
    public int Level;

    public override void Awake()
    {
        base.Awake();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void OnInited()
    {
        base.OnInited();

        MessageManager.Instance.Init();
    }

    public void InitGame()
    {
        UIManager.Instance.ShowScreen<ScreenPlay>();
        UIManager.Instance.ShowPopup<PopupGuide>();

        map.Init();
    }

    public void StartGame()
    {
        map.StartGame();
    }

    

   


}
