﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageManager : SingletonBasic<MessageManager>
{
    public Dictionary<Type, GroupMessage> listeners = new Dictionary<Type, GroupMessage>();

    public override void OnInited()
    {
        base.OnInited();

    }

    public void Register(Type key, Action<BaseMessage> action, int id)
    {
        if (!listeners.ContainsKey(key))
        {
            listeners.Add(key, new GroupMessage());
        }

        if (listeners[key] != null)
        {
            listeners[key].Attach(action,id);
        }
    }

    public void Register<T>(Action<T> action) where T : BaseMessage
    {
        var key = typeof(T);
        Register(key, (message) => action(message as T),action.GetHashCode());
    }

    public void Unregister(Type key, Action<BaseMessage> action,int id)
    {
        if (listeners.ContainsKey(key) && listeners[key] != null)
        {
            listeners[key].Detach(action,id);
        }
    }

    public void Unregister<T>(Action<T> action) where T : BaseMessage
    {
        var key = typeof(T);
        Unregister(key, (message) => action(message as T),action.GetHashCode());
    }

    //public void UnregisterAll(Action<BaseMessage> action)
    //{
    //    foreach (Type key in listeners.Keys)
    //    {
    //        Unregister(key, action);
    //    }
    //}

    public void Broadcast<T>(T val) where T : BaseMessage
    {
        if (val == null)
        {
            Debug.LogError(" Broadcast val nulll");
            return;
        }
        var key = typeof(T);

        if (listeners.ContainsKey(key) && listeners[key] != null)
        {
            listeners[key].Broadcast(val);
        }
    }

    public void Clear()
    {
        listeners.Clear();
    }
}


public class GroupMessage
{
    List<AcionMessage> actions = new List<AcionMessage>();

    public void Broadcast(BaseMessage value)
    {
        for (int i = 0; i < actions.Count; i++)
        {
            if (actions[i] == null)
                continue;
            actions[i].Broadcast(value);
        }
    }

    public void Attach(Action<BaseMessage> atc, int id)
    {
        for (int i = 0; i < actions.Count; i++)
        {
            if (actions[i].Id == id)
                return;
        }
        actions.Add(new AcionMessage()
        {
            Id = id,
            Action = atc
        });
    }

    public void Detach(Action<BaseMessage> action,int id)
    {
        for (int i = 0; i < actions.Count; i++)
        {
            if (actions[i].Id == id)
            {
                actions.Remove(actions[i]);
                break;
            }
        }
    }
}

public class AcionMessage
{
    public int Id;
    public Action<BaseMessage> Action;

    public void Broadcast(BaseMessage value)
    {
        if (Action != null)
        {
            Action(value);
        }
    }
}
