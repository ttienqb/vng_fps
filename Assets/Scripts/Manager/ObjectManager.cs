﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectManager : Singleton<ObjectManager>
{
    public Transform tfPool;
    public BaseObject[] listPrefab;

    private List<BaseObject> poolObj = new List<BaseObject>();
    private const int MAX_POOL_SIZE = 50;
    private int count = 0; // for test

    public override void Awake()
    {
        base.Awake();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void OnInited()
    {
        base.OnInited();
        HideAllPrefabs();
        poolObj.Clear();
    }

    public void ApplyPool(BaseObject val)
    {
        val.Clear();
        val.gameObject.SetActive(false);
        val.transform.SetParent(this.tfPool);

        if (!this.poolObj.Contains(val))
        {
            this.poolObj.Add(val);
        }

        if(this.poolObj.Count > MAX_POOL_SIZE)
        {
            BaseObject removeScr = this.poolObj[0];
            Destroy(removeScr.gameObject);
            this.poolObj.RemoveAt(0);
        }
    }

    public BaseObject SpawnNewObject(ObjectType ty)
    {
        BaseObject result = null;
        for (int i = 0; i < this.poolObj.Count; i++)
        {
            if(this.poolObj[i].ObjType == ty)
            {
                result = this.poolObj[i];
                this.poolObj.RemoveAt(i);
                break;
            }
        }

        if(result == null)
        {
            for (int i = 0; i < this.listPrefab.Length; i++)
            {
                if (this.listPrefab[i].ObjType == ty)
                {
                    GameObject ob = Instantiate(this.listPrefab[i].gameObject) as GameObject;
                    ob.name = ty + "_" + this.count;
                    result = ob.GetComponent<BaseObject>();
                    this.count++;
                    break;
                }
            }
        }

        if(result != null)
        {
            result.Clear();
            result.gameObject.SetActive(true);
        }

        return result;
    }

    private void HideAllPrefabs()
    {
        for (int i = 0; i < this.listPrefab.Length; i++)
        {
            this.listPrefab[i].gameObject.SetActive(false);
        }
    }

}
