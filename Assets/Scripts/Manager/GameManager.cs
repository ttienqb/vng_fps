﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class GameManager : Singleton<GameManager>
{

    public override void Awake()
    {
        base.Awake();
    }

    void Start()
    {
        // start game
        LevelManager.Instance.InitGame();
    }

    public override void OnDestroy()
    {
        base.OnDestroy();
    }

    public override void OnInited()
    {
        base.OnInited();

        AudioManager.Instance.Init();
        InputManager.Instance.Init();
    }


}
