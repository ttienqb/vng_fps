﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    public PlayerObject player;
    public GameStateSwitch switchState;
    public GameStatePause pauseState;
    public GameStatePlay playState;
    public GameStateEnd endState;
    public WaveObject[] waves;

    private int curWave;
    private FSM fsm;

    public int CurWave { get => curWave; }
    public FSM Fsm { get => fsm; }

    void Awake()
    {
        MessageManager.Instance.Init();
    }

    void Start()
    {

    }

    void Update()
    {
        if (this.fsm != null)
        {
            this.fsm.OnUpdate(Time.deltaTime);
        }
    }

    void OnDestroy()
    {

    }

    public void Init()
    {
        Clear();
        for (int i = 0; i < waves.Length; i++)
        {
            waves[i].Init();
        }
        InitFSM();
    }

    public void Clear()
    {
        if (this.fsm != null)
        {
            this.fsm.Clear();
            this.fsm = null;
        }
        this.curWave = 1;
    }

    public void StartGame()
    {
        fsm.Start(Constant.FSM_TRANSITION_GAME_SWITCH);
    }

    private void InitFSM()
    {
        // FSM
        fsm = new FSM();

        switchState.Init(fsm, Constant.FSM_TRANSITION_GAME_SWITCH, this);
        playState.Init(fsm, Constant.FSM_TRANSITION_GAME_PLAY, this);
        pauseState.Init(fsm, Constant.FSM_TRANSITION_GAME_PAUSE, this);
        endState.Init(fsm, Constant.FSM_TRANSITION_GAME_END, this);

        // add transitions
        switchState.RegisterTransition(Constant.FSM_TRANSITION_GAME_PLAY, playState);
        switchState.RegisterTransition(Constant.FSM_TRANSITION_GAME_PAUSE, pauseState);
        switchState.RegisterTransition(Constant.FSM_TRANSITION_GAME_END, endState);

        playState.RegisterTransition(Constant.FSM_TRANSITION_GAME_SWITCH, switchState);
        playState.RegisterTransition(Constant.FSM_TRANSITION_GAME_PAUSE, pauseState);
        playState.RegisterTransition(Constant.FSM_TRANSITION_GAME_END, endState);

        pauseState.RegisterTransition(Constant.FSM_TRANSITION_GAME_SWITCH, switchState);
        pauseState.RegisterTransition(Constant.FSM_TRANSITION_GAME_PLAY, playState);
        pauseState.RegisterTransition(Constant.FSM_TRANSITION_GAME_END, endState);

        endState.RegisterTransition(Constant.FSM_TRANSITION_GAME_PLAY, playState);
        endState.RegisterTransition(Constant.FSM_TRANSITION_GAME_PAUSE, pauseState);
        endState.RegisterTransition(Constant.FSM_TRANSITION_GAME_SWITCH, switchState);

        fsm.AddState(switchState);
        fsm.AddState(playState);
        fsm.AddState(pauseState);
        fsm.AddState(endState);

    }

    public void ResumeGame()
    {
        fsm.Start(Constant.FSM_TRANSITION_GAME_PLAY);
    }

    public void CompleteWave()
    {
        if (this.player.CurHp <= 0)
            return;
        AudioManager.Instance.StopAllSounds();
        this.curWave++;
        if (this.curWave <= this.waves.Length)
        {

            AudioManager.Instance.PlaySound(SoundGame.Sound_Clear);
            fsm.Start(Constant.FSM_TRANSITION_GAME_SWITCH);
        }
        else
        {
            AudioManager.Instance.PlaySound(SoundGame.Sound_Victory);
            fsm.Start(Constant.FSM_TRANSITION_GAME_END);
        }
    }

    public void GameOver()
    {
        AudioManager.Instance.StopAllSounds();
        AudioManager.Instance.PlaySound(SoundGame.Sound_Lose);
        fsm.Start(Constant.FSM_TRANSITION_GAME_END);
    }

    public WaveObject GetCurWave()
    {
        return this.waves[this.curWave - 1];
    }

}
