﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public FPSCamera fpsCam;
    public PlayerObject Player;
    public AKWeaponState akState;
    public GunWeaponState gunState;

  
    private FSM fsm;

    public void Init()
    {
        InitFSM();
    }

    public void Clear()
    {
        if (this.fsm != null)
        {
            this.fsm.Clear();
            this.fsm = null;
        }
    }

    public void OnUpdate(float dt)
    {
        if (this.fsm != null)
        {
            this.fsm.OnUpdate(dt);
        }
    }

    public void Fire()
    {
        WeaponFSMState wState = (WeaponFSMState)this.fsm.CurState;
        if(wState != null)
        {
            wState.Fire();
        }
    }

    public void Reload()
    {
        WeaponFSMState wState = (WeaponFSMState)this.fsm.CurState;
        if (wState != null)
        {
            wState.Reload();
        }
    }

    public void Switch()
    {
        AudioManager.Instance.PlaySound(SoundGame.Sound_Switch);

        if (fsm.CurState != null
            && fsm.CurState.Key.Equals(Constant.FSM_TRANSITION_WEAPON_AK))
        {
            fsm.ChangeStateByKey(Constant.FSM_TRANSITION_WEAPON_GUN);
        }
        else
        {
            fsm.ChangeStateByKey(Constant.FSM_TRANSITION_WEAPON_AK);
        }
    }

    public void ChargeFull()
    {
        WeaponFSMState wState = (WeaponFSMState)this.fsm.CurState;
        if (wState != null)
        {
            wState.ChargeFull();
        }
    }

    public void SetOnFPSCam(bool isOn)
    {
        WeaponFSMState wState = (WeaponFSMState)this.fsm.CurState;
        if (wState != null)
        {
            wState.SetOnFPSCam(isOn);
        }
    }

    private void InitFSM()
    {
        fsm = new FSM();

        akState.Init(fsm, Constant.FSM_TRANSITION_WEAPON_AK, this);
        gunState.Init(fsm, Constant.FSM_TRANSITION_WEAPON_GUN, this);

        akState.RegisterTransition(Constant.FSM_TRANSITION_WEAPON_GUN, gunState);
        gunState.RegisterTransition(Constant.FSM_TRANSITION_WEAPON_AK, akState);

        fsm.AddState(akState);
        fsm.AddState(gunState);

        fsm.Start(Constant.FSM_TRANSITION_WEAPON_GUN);
    }

}
