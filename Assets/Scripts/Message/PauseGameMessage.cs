﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseGameMessage : BaseMessage
{
    public bool IsPaused;
}
