﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyDamageMessage : BaseMessage
{
    public int Damage;
}
