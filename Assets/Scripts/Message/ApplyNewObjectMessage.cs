﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplyNewObjectMessage : BaseMessage
{
    public BaseObject Target;
}
