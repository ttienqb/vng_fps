﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIHpMessage : BaseMessage
{
    public int CurHp;
    public int TotalHp;
}
