﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerObject : DynamicObject
{
    public HUDController hud;
    public WeaponController weapon;
    public int DefaultHp = 10;

    private float fireTime;
    private float reloadTime;
    private PlayerState myState;
    private int curHp;
    private float blockAtkTime;

    public int CurHp { get => curHp;  }

    public override void Init()
    {
        if (MessageManager.HasInstance)
        {
            Subscribe<InputMessage>(OnListen_Input);
            Subscribe<ApplyDamageMessage>(OnListen_ApplyDamage);
        }

        this.fireTime = 0;
        this.myState = PlayerState.Idle;
        this.reloadTime = Constant.CHAR_TIME_RELOAD;
        this.curHp = this.DefaultHp;

        weapon.Init();

        SetOnFPSCam(false);
    }

    public override void BeHit(BaseObject objShoot)
    {

    }

    public override void Shoot(BaseObject objBeHit)
    {

    }

    public override void Clear()
    {
        if (MessageManager.HasInstance)
        {
            Unsubscribe<InputMessage>(OnListen_Input);
            Unsubscribe<ApplyDamageMessage>(OnListen_ApplyDamage);
        }
        weapon.Clear();
    }

    public override void OnUpdate(float dt)
    {
        fireTime -= dt;
        if (this.blockAtkTime > 0)
        {
            this.blockAtkTime -= dt;
        }

        switch (this.myState)
        {
            case PlayerState.Idle:
                break;
            case PlayerState.Reload:
                break;
            case PlayerState.Switch:
                break;
            case PlayerState.Fire:
                break;
        }

        this.weapon.OnUpdate(dt);
    }

    void OnListen_Input(InputMessage msg)
    {
        //Debug.Log(" OnListen_Input " + msg.NameInput);
        if (msg.NameInput == Constant.INPUT_FIRE)
        {
            OnFire();
        }
        else if (msg.NameInput == Constant.INPUT_RELOAD)
        {
            OnReloadWeapon();
        }
        else if (msg.NameInput == Constant.INPUT_SWITCH)
        {
            OnSwitchWeapon();
        }
        else { }
    }

    void OnListen_ApplyDamage(ApplyDamageMessage msg)
    {
        if (this.blockAtkTime > 0)
            return;
        AudioManager.Instance.PlaySound(SoundGame.Sound_BeHit);

        this.blockAtkTime = Constant.CHAR_TIME_BLOCK_ATK;
        this.curHp -= msg.Damage;
        Broadcast(new UIHpMessage()
        {
            CurHp = this.curHp,
            TotalHp = this.DefaultHp
        });
        EffectManager.Instance.ApplyDamageEffect();

        if (this.curHp <= 0)
        {
            Broadcast(new AnyMessage()
            {
                Name = Constant.MESSAGE_ANY_PLAYER_DIE
            });
        }
    }

    private void OnUpdateByReload(float dt)
    {
        this.reloadTime -= dt;
        if (this.reloadTime <= 0)
        {
            this.myState = PlayerState.Idle;
            ChargeFullBullet();
        }
    }

    private void OnFire()
    {
        if (this.fireTime > 0)
            return;
        this.fireTime = Constant.CHAR_TIME_FIRE;
        weapon.Fire();

    }

    private void OnReloadWeapon()
    {
        weapon.Reload();
    }

    private void OnSwitchWeapon()
    {
        weapon.Switch();
    }

    private void ChargeFullBullet()
    {
        weapon.ChargeFull();
    }

    public void SetOnFPSCam(bool isOn)
    {
        this.hud.gameObject.SetActive(isOn);
        this.Fbx.SetActive(!isOn);
        this.weapon.SetOnFPSCam(isOn);

        if (isOn)
        {
            Broadcast(new UIHpMessage()
            {
                CurHp = this.curHp,
                TotalHp = this.DefaultHp
            });
        }
    }

    public void SetAnimation(string trigger)
    {
        this.Ani.SetTrigger(trigger);
    }


}
