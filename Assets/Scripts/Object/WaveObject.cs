﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class WaveObject : StaticObject
{
    public Transform CharMovePoint;
    public Transform[] spawnPoints;
    public float delayTime;
    public int walkEnenimes;
    public int gunEnenimes;

    private float cacheTime = 0;
    private List<BaseObject> objects;
    private int walkCache;
    private int gunCache;

    void OnEnable()
    {
        if (MessageManager.HasInstance)
        {
            MessageManager.Instance.Register<EnemyDieMessage>(OnListen_EnemyDie);
        }
    }

    void OnDisable()
    {
        if (MessageManager.HasInstance)
        {
            MessageManager.Instance.Unregister<EnemyDieMessage>(OnListen_EnemyDie);
        }
    }

    public override void Init()
    {
        cacheTime = 0;
        this.gameObject.SetActive(false);
        this.objects = new List<BaseObject>();
    }

    public override void OnUpdate(float dt)
    {
        if (walkEnenimes > 0 || gunEnenimes > 0)
        {
            this.cacheTime += Time.deltaTime;
            if (cacheTime >= delayTime)
            {
                SpawnEnemy();
                cacheTime = 0;
            }
        }

        for (int i = 0; i < this.objects.Count; i++)
        {
            if (objects[i] != null)
            {
                this.objects[i].OnUpdate(Time.deltaTime);
            }
        }
    }

    public override void BeHit(BaseObject objShoot)
    {
    }

    public override void Shoot(BaseObject objBeHit)
    {
    }

    public override void Clear()
    {
    }

    void OnListen_EnemyDie(EnemyDieMessage val)
    {
       
        this.objects.Remove(val.Target);
        val.Target.ObjectDestroy();

        if (val.Target.ObjType == ObjectType.Enemy)
        {
            this.walkCache--;
        }
        else if (val.Target.ObjType == ObjectType.Enemy_Gun)
        {
            this.gunCache--;
        }
        else { }

        Broadcast_MissionUI();

        if (this.walkCache <= 0 && this.gunCache <= 0)
        {
            if (this.objects.Count <= 0)
            {
                Broadcast(new AnyMessage()
                {
                    Name = Constant.MESSAGE_ANY_COMPLETE_WAVE
                });
            }
            ActiveWave(false);
        }
    }

    public void SpawnEnemy()
    {
        BaseObject scr = null;

        Transform rnPoint = this.spawnPoints[Random.Range(0, this.spawnPoints.Length)];

        if (this.walkEnenimes > 0)
        {
            if (this.gunEnenimes <= 0 || Random.Range(0, 2) == 1)
            {
                scr = ObjectManager.Instance.SpawnNewObject(ObjectType.Enemy);
                this.walkEnenimes--;
            }
            
        }

        if (scr == null && this.gunEnenimes > 0)
        {
            scr = ObjectManager.Instance.SpawnNewObject(ObjectType.Enemy_Gun);
            this.gunEnenimes--;
        }

        if (scr != null)
        {
            scr.transform.SetParent(rnPoint);
            scr.transform.localPosition = Vector3.zero;
            scr.Init();
            this.objects.Add(scr);
        }

    }

    public void ActiveWave(bool isOn)
    {
        this.gameObject.SetActive(isOn);

        if (isOn)
        {
            this.walkCache = this.walkEnenimes;
            this.gunCache = this.gunEnenimes;

            Broadcast_MissionUI();
        }
    }

    private void Broadcast_MissionUI()
    {
        Broadcast(new UIMissionMessage()
        {
            GunEnemies = this.gunCache,
            WalkEnemies = this.walkCache,
        });
    }

}
