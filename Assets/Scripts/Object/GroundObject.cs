﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundObject : StaticObject
{
    public override void BeHit(BaseObject objShoot)
    {
    }

    public override void Shoot(BaseObject objBeHit)
    {
    }

    public override void Clear()
    {
    }

    public override void OnUpdate(float dt)
    {
    }

    public override void Init()
    {

    }
}
