﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicObject : BaseObject
{
    public Animator Ani;

    public override void BeHit(BaseObject objShoot)
    {
    }

    public override void BeHitByHeadShot(BaseObject objShoot)
    {

    }

    public override void Shoot(BaseObject objBeHit)
    {
    }

    public override void Clear()
    {
    }

    public override void Init()
    {
    }

    public override void InitByData(float x, float z)
    {
        base.InitByData(x, z);
    }

    public override void ObjectDestroy()
    {
        base.ObjectDestroy();
    }

    public override void OnUpdate(float dt)
    {

    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }

    public override bool Equals(object other)
    {
        return base.Equals(other);
    }

    public override string ToString()
    {
        return base.ToString();
    }

    
}
