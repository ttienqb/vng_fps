﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseObject : MonoBehaviour
{
    public ObjectType ObjType;
    public CollisionType CollisionType;
    public GameObject Fbx;

    public virtual void OnTriggerEnter(Collider other)
    {
        BaseObject shootObj = other.GetComponent<BaseObject>();
        if (shootObj == null)
            return;

        shootObj.Shoot(this);
        this.BeHit(shootObj);
    }

    public abstract void Init();
    public abstract void OnUpdate(float dt);
    public abstract void BeHit(BaseObject objShoot);
    public abstract void BeHitByHeadShot(BaseObject objShoot);
    public abstract void Shoot(BaseObject objBeHit);
    public abstract void Clear();

    public virtual void ObjectDestroy()
    {
        ObjectManager.Instance.ApplyPool(this);
    }

    public virtual void InitByData(float x, float z)
    {
        this.transform.position = new Vector3(x, 0, z);
    }


    public void Broadcast<T>(T val) where T : BaseMessage
    {
        if (val == null)
            return;
        MessageManager.Instance.Broadcast(val);
    }

    public void Subscribe<T>(Action<T> excute) where T : BaseMessage
    {
        var key = typeof(T);
        MessageManager.Instance.Register(key, (message) => excute(message as T), excute.GetHashCode());
    }

    public void Unsubscribe<T>(Action<T> excute) where T : BaseMessage
    {
        var key = typeof(T);
        MessageManager.Instance.Unregister(key, (message) => excute(message as T), excute.GetHashCode());
    }

}
