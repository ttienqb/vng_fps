﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyObject : DynamicObject
{
    public Transform player;
    public HealthBar HpBar;
    public int DefaultHp = 4;
    public int Damage = 1;

    protected int Hp;

    private EnemyState myState;
    private float timeAtk;


    public override void Init()
    {
        this.Hp = this.DefaultHp;
        this.myState = EnemyState.Move;
        HpBar.Init(this.Hp);
    }

    public override void Clear()
    {
    }

    public override void BeHit(BaseObject objShoot)
    {
        switch (objShoot.CollisionType)
        {
            case CollisionType.Player:
                {
                    this.Hp--;
                    if (this.Hp <= 0)
                    {
                        this.myState = EnemyState.Die;
                        Broadcast(new EnemyDieMessage() { Target = this });
                    }
                    HpBar.SetHp(this.Hp);
                    KnockBack(EnemyState.Move);
                    this.myState = EnemyState.KnockBack;
                }
                break;
        }
    }

    public override void BeHitByHeadShot(BaseObject objShoot)
    {
        switch (objShoot.CollisionType)
        {
            case CollisionType.Player:
                {
                    AudioManager.Instance.PlaySound(SoundGame.Sound_Headshot);
                    this.Hp = 0;
                    HpBar.SetHp(this.Hp);
                    this.myState = EnemyState.Die;
                    Broadcast(new EnemyDieMessage() { Target = this });
                }
                break;
        }
    }

    public override void Shoot(BaseObject objBeHit)
    {
    }

    public override void OnUpdate(float dt)
    {
        switch (this.myState)
        {
            case EnemyState.Idle:
                {
                    OnUpdateByAtk(dt);
                }
                break;
            case EnemyState.Move:
                {
                    OnUpdateByMove(dt);
                }
                break;
            case EnemyState.Fire:
                {
                }
                break;
            case EnemyState.KnockBack:
                {
                }
                break;
            case EnemyState.Die:
                break;
            case EnemyState.Atk:
                {
                    OnUpdateByAtk(dt);
                }
                break;
        }

        OnUpdateByAll(dt);
    }

    protected virtual void OnUpdateByAll(float dt)
    {
    }

    private void OnUpdateByAtk(float dt)
    {
        this.timeAtk -= dt;
        if (this.timeAtk <= 0)
        {
            this.timeAtk = Constant.ENEMY_TIME_ATK;
            Broadcast(new ApplyDamageMessage()
            {
                Damage = this.Damage
            });
        }
    }

    private void OnUpdateByMove(float dt)
    {
        float distance = Vector3.Distance(this.transform.position, this.player.position);
        if (distance < Constant.ENEMY_SPACE_ATK)
        {
            this.myState = EnemyState.Atk;
            this.timeAtk = Constant.ENEMY_TIME_ATK;
            return;
        }

        Vector3 dirVc = CalculateDir();
        if (dirVc == Vector3.zero)
            return;
        transform.position += dirVc * dt * Constant.ENEMY_SPEED;
        MyUtils.LookAt2D(this.transform, dirVc);

    }

    public virtual Vector3 CalculateDir()
    {
        // avoid object surround
        Vector3 resultVc = (this.player.position - this.transform.position).normalized;
        List<Transform> nearObjs = new List<Transform>();
        Collider[] colliders = Physics.OverlapSphere(transform.position, Constant.ENEMY_AVOID);
        if (colliders != null)
        {
            for (int i = 0; i < colliders.Length; i++)
            {
                BaseObject scr = colliders[i].GetComponent<BaseObject>();
                if (scr != null)
                {
                    if (scr.CollisionType == CollisionType.Player
                        || scr.CollisionType == CollisionType.Enemy
                         || scr.CollisionType == CollisionType.Ground)
                        continue;
                }
                nearObjs.Add(colliders[i].transform);
            }
        }

        if (nearObjs.Count > 0)
        {
            Vector3 avoidanceMove = Vector3.zero;
            for (int i = 0; i < nearObjs.Count; i++)
            {
                avoidanceMove += (this.transform.position - nearObjs[i].position).normalized;
            }

            resultVc = avoidanceMove / nearObjs.Count;
        }

        return new Vector3(resultVc.x, 0, resultVc.z);
    }

    private void KnockBack(EnemyState cacheState)
    {
        Vector3 vc = -(this.player.position - this.transform.position).normalized;
        vc.y = 0;
        MyUtils.LookAt2D(this.transform, -vc);

        DOTween.Kill(this.GetInstanceID().ToString());
        Sequence seq = DOTween.Sequence();
        seq.SetId(this.GetInstanceID().ToString());

        float dt = 0.4f;
        float distance = Constant.ENEMY_KNOCK_DISTANCE;
        Vector3 cachePos = transform.position;
        seq.Append(this.transform.DOMove(vc * distance + cachePos, dt).SetEase(Ease.OutCirc));
        seq.Append(this.transform.DOMove(vc * distance * 0.5f + cachePos, dt * 0.3f));
        seq.OnComplete(delegate ()
        {
            this.myState = cacheState;
        });
    }

    
}
