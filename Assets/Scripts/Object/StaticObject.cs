﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticObject : BaseObject
{
    public override void BeHit(BaseObject objShoot)
    {
    }

    public override void BeHitByHeadShot(BaseObject objShoot)
    {
    }

    public override void Shoot(BaseObject objBeHit)
    {
    }

    public override void Clear()
    {
    }

    public override void Init()
    {
    }

    public override void InitByData(float x, float z)
    {
        base.InitByData(x, z);
    }

    public override void ObjectDestroy()
    {
        base.ObjectDestroy();
    }

    public override void OnUpdate(float dt)
    {

    }

    
}